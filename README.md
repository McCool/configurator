# Configurator

Utility class that reads simple "key = value" configuration files and provides a dictionary interface for accessing the key-indexed values found.