# Configurator

# This class reads a configuration file with the following form:
# 1. Each line is a key-value pair, separated by a delimiter (' ' by default).
# 2. Trailing and leading whitespace around both key and value are ignored.
# 3. Lines with '#' as the first non-whitespace char are ignored.
# 4. Lines without a delimiter character are also ignored.
# 5. Options are read sequentially; later options override earlier options.
# 6. Quotes are NOT necessary for values, and will be included in values.

# Example configuration file:
# title      Configurator: The Class That Reads Configuration Files
# author     Cool McCool Labs
# date       2019/03/15
# publisher  Incredible books co.
# pages      42
# rating     5 stars
# notes      This is just an example - you can use any key-value pairs!

import os

class Configurator :

    # Initialize the Configurator by reading the given configuration file
    def __init__ (self, filename : str, delimiter : str = " ") :
        # Create a new empty dictionary that holds key-value pairs
        self.options = {}
        # Make sure the file exists
        if not os.path.isfile(filename) :
            raise ValueError("Config file: '" + filename + "' does not exist, or is not a regular file")
        # Loop through each line of the config file, stripping leading and trailing whitespace
        with open(filename) as f :
            for line in f :
                pair = line.split(delimiter, 1) # Split the line around left-most delimiter
                if (len(pair) < 2) : continue   # This line had no delimiter
                key = pair[0].strip()           # Strip whitespace from key
                if (key is '') : continue       # The key was all whitespace
                if (key[0] is '#') : continue   # The line was a comment
                val = pair[1].strip()           # Strip whitespace from value
                if (val is '') : continue       # The value was all whitespace
                self.options[key] = val;        # Set the key-value pair

    # Getter method that returns the value associated with the given key
    def get (self, key : str) :
        try :
            return self.options[key]
        except :
            return None

if __name__ == "__main__" :
    config = Configurator("config.txt", " ")
    print(config.options)
